
#1. Create a text file with some lines of text. Write a Pyhton program to open the file, read its contents,
# and print each line.
file = open("text.txt", "r")
#print(file.read())  print the whole text
print(file.readline())       # print the text line by line
print(file.readline())
print(file.readline()) 
file.close()

print("\nPrint the text line by line")
file1 = open("text.txt", "r")
for i in file1:               # print the text line by line
    print(i)
file1.close()

#2. Prompt the user to enter a series of lines of text. 
# Write a Python program to create a new text file and write the entered lines to the file.

input_text=input("Please enter some text: ")
file3 = open("demofile.txt", "w")
file3.write(input_text)
file3.close()
file4 = open("demofile.txt", "r")
print(file4.read())
file4.close()

#3. Read a text file and count the number of words in it. Print the total word count.
words_counter = 0
file5=open("text.txt", 'r')
for j in file5:
    word = j.split()
    words_counter += len(word)
print("The number of words in the 'text' file is: ",words_counter )

#4. Creat a program that copies the contents of one text file to another. Prompt the user for the file name.
file_name=input("Please enter the file name which text do you want to copy: ")
file6=open(file_name,'r')
file7=open("new_file.txt",'w')
file7.write(file6.read())
#5. Create a Python program that reads a file containing names and grades of students.
 #The program should then prompt the user to input new grades for each student and append these new grades 
 #to each corresponding line in the file. Finally, the updated information should be written to a new file.
file8=open('Name_grades.txt','r')
data = file8.readlines() 
for a in range(len(data)-1): 
    line = data[a]
    name=input("Please enter the student name: ")
    if name in line: 
        new_grade=input(f"Please enter the new grade for {name}: ")
        data[a] = line.replace(data[a][-4:],new_grade)
file9=open('Name_new_grades.txt','w',newline='\n') 
for line in range(len(data)):
    file9.writelines(data[line]) 
    file9.write('\n')